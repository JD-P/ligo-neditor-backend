import os
import unittest

from wrap_ligo import app

PASCALIGO_SAMPLE = """
function main (const parameter : int;  const contractStorage : int) : (list(operation) * int) is \n block {skip} with ((nil : list(operation)), contractStorage + parameter)
"""

CAMELIGO_SAMPLE = """
type storage = int * int list

type param = int list

let%entry main (p : param) storage =
  let storage =
    match p with
      [] -> storage
    | hd::tl -> storage.(0) + hd, tl
  in (([] : operation list), storage)
"""

class DryRunTests(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        self.client = app.test_client()

    def test_taco_contract(self):
        compile_response = self.client.post("/api/0.1/dry-run",
                                            json={
                                                "program":PASCALIGO_SAMPLE,
                                                "entrypoint":"main",
                                                "param_expr":"3",
                                                "stor_expr":"4"})
        # TODO: Find better way to test that we get result we're looking for
        self.assertTrue('7' in compile_response.get_json()["out"])

    def test_cameligo_syntax(self):
        """Test that the CameLIGO syntax is usable from the API."""
        compile_response = self.client.post("/api/0.1/dry-run",
                                            json={
                                                "program":CAMELIGO_SAMPLE,
                                                "entrypoint":"main",
                                                "param_expr":"[3]",
                                                "stor_expr":"(3,[3])",
                                                "syntax":"cameligo",
                                                })
        self.assertTrue("unsupported pattern-matching"
                        in compile_response.get_json()["out"])
        
    def test_too_big_program(self):
        compile_response = self.client.post("/api/0.1/dry-run",
                                            json={
                                                "program":"Z" * 2000000,
                                                "entrypoint":"none",
                                                "param_expr":"I troll you. :3",
                                                "stor_expr":"trolololol"})
        self.assertEqual(compile_response.status, "400 BAD REQUEST")

class ContractCompileTests(unittest.TestCase):
    def setUp(self):
        app.config["TESTING"] = True
        app.config["DEBUG"] = False
        self.client = app.test_client()

    def test_taco_contract(self):
        compile_response = self.client.post("/api/0.1/compile-contract",
                                            json={
                                                "program":PASCALIGO_SAMPLE,
                                                "entrypoint":"main",
                                                "syntax":"pascaligo"})
        compiled_program = compile_response.get_json()["out"]
        # TODO: Find better way to test that we get result we're looking for
        self.assertTrue("DIP" in compiled_program and
                        "DUP" in compiled_program and
                        "DROP" in compiled_program)

    def test_cameligo_sample(self):
        """Test that we can compile a contract using cameligo syntax."""
        compile_response = self.client.post("/api/0.1/compile-contract",
                                            json={
                                                "program":CAMELIGO_SAMPLE,
                                                "entrypoint":"main",
                                                "syntax":"cameligo",})
        print("test_cameligo_sample (TODO): Sample doesn't compile yet.")

        
if __name__ == "__main__":
    unittest.main()
