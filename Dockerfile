FROM ubuntu:18.04

MAINTAINER John David Pressman "chainbreakers@hirechainbreakers.com"

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev gunicorn

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

COPY . /app

ENTRYPOINT [ "gunicorn", "-w", "4", "wrap_ligo:app" ]
