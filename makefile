.ONESHELL:
SHELL := /bin/bash

wrap_ligo:
	virtualenv --python=python3 env_neditor
	source env_neditor/bin/activate
	pip install -r requirements.txt
	pip install gunicorn
	echo -e "\e[32m\nUse 'source env_neditor/bin/activate' to set env variables/etc\e[0m"
	echo -e "\e[32mThen run 'gunicorn -w 4 wrap_ligo:app' to start the backend.\e[0m"
